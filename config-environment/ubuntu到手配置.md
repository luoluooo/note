#### ubuntu分区策略

```
EFI系统分区 500MB  #GPT硬盘格式
Boot系统分区 500MB #MBR硬盘格式
swap分区  #一般比内存大个几G
/  		#根目录区 相当于windows C盘
/home    #用户区，相当于windows D盘
记得下面选择驱动盘的时候将efi设为引导盘，若出现efi系统分区无法在下方显示出，请去掉efi系统分区

```

#### 安装nvidia显卡步骤

```bash
在nvidia官网下载对应ubuntu驱动
sudo apt update
sudo apt install build-essential  #安装gcc编译器
sudo apt-get install lib32z1 lib32ncurses5  #安装NVIDIA32位兼容库  如果是ubuntu20.xx往上版本，改lib32ncurses5为lib32ncurses5-dev
sudo ./NVIDIAxxxx.run --no-x-check  #在下载目录下执行该条语句 --no-x-check 代表不检查图形界面，不加的话会报本机有图形界面在运行错误。

#可能会报nouveau在运行错误，按以下操作进行即可
sudo gedit /etc/modprobe.d/blacklist.conf
#打开文件，在文件末尾写入：
blacklist nouveau
options nouveau modeset=0
#保存后手动更新；
sudo update-initramfs -u
#电脑重启，输入下列指令进行确认，若无输出，则禁用成功：
lsmod | grep nouveau
```

#### 安装cuda步骤

```bash
https://developer.nvidia.com/cuda-downloads #打开nvidia-cudatoolkit，如果已安装nvidia驱动，记得安装时把驱动项去掉

export PATH="$PATH:/usr/local/cuda-10.1/bin"
export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:/usr/local/cuda-10.1/lib64/"
export LIBRARY_PATH="$LIBRARY_PATH:/usr/local/cuda-10.1/lib64"  #写入环境变量(.bashrc)，注意更改文件夹名字 如cuda-10.1改为cuda
source .bashrc

```

#### 安装cudnn步骤

```sh
https://developer.nvidia.com/rdp/cudnn-archive #官网下载tar包
tar -xvf cudnn-10.1-linux-x64-v7.6.5.32.tgz #解压
sudo cp cuda/include/cudnn.h /usr/local/cuda/include/
sudo cp cuda/lib64/libcudnn* /usr/local/cuda/lib64/	 #将cuda/include/cudnn.h文件复制到usr/local/cuda/include文件夹，将cuda/lib64/下所有文件复制到/usr/local/cuda/lib64文件夹中：
sudo chmod a+r /usr/local/cuda/include/cudnn.h
sudo chmod a+r /usr/local/cuda/lib64/libcudnn*	#添加读取权限

sudo rm -rf /usr/local/cuda    #删除之前创建的软链接
sudo ln -s /usr/local/cuda-9.0 /usr/local/cuda    #创建链接到cuda9.0的软链接
nvcc --version    #查看当前cuda版本  多版本cuda安装可参考该上命令，创建对应软连接即可


https://blog.csdn.net/weixin_44120025/article/details/121002696
```



#### ubuntu到手配置

 版本号查看: ***lsb_release -a***

换源教程链接:

```
https://blog.csdn.net/qq_51468843/article/details/115681456
```

##  ros安装:

### 执行完换源后，参考下方ros安装教程

```
https://blog.csdn.net/qq_57162593/article/details/118532403 
```

### or鱼香ros一键安装（推荐）

```bash
wget http://fishros.com/install -O fishros && . fishros
```

## ubuntu换中文教程:

```
https://www.jb51.net/os/Ubuntu/657328.html
```

## ubuntu换中文输入法教程:

```bash
https://blog.csdn.net/weixin_43431593/article/details/106444769 #完成上一步后从第五步开始做起
```

## ubuntu科学上网流程

### 方法一

```bash
https://blog.csdn.net/weixin_45934869/article/details/129100574
https://rustintherubble.github.io/posts/2022/09/virtualbox-proxy/ #可只开放socks主机，v2ray的端口号为10810 如果要git—clone下载 http端口号也需开放 为10811
```

![鱼香ros一键安装（推荐）](./../images/image-20230306133532377.png)

win11端口开放

```
https://blog.csdn.net/weixin_42727710/article/details/122495314
```

### 方法二

https://zhuanlan.zhihu.com/p/414998586

```bash
#安装 V2Ray 内核：
#v2rayA 提供的镜像脚本（推荐）
curl -Ls https://mirrors.v2raya.org/go.sh | sudo bash
#安装后可以关掉服务，因为 v2rayA 不依赖于该 systemd 服务
sudo systemctl disable v2ray --now
#https://github.com/v2rayA/v2rayA/releases 进入该链接下载installer_debian_amd64_2.0.5.deb并进行安装
sudo apt install /path/download/installer_debian_xxx_vxxx.deb # 自行替换 deb 包所在的实际路径
#启动 v2rayA 
sudo systemctl start v2raya.service
#设置开机自动启动
sudo systemctl enable v2raya.service
#在设置中开启大陆白名单模式
```

安装完成之后进入http://localhost:2017/粘贴订阅地址

![Screenshot from 2023-03-07 00-25-29](../images/Screenshot%20from%202023-03-07%2000-25-29.png)







