#### Git安装

在https://git-scm.com 下载64位安装包

![image-20230313223410151](./../images/Git%E5%AE%89%E8%A3%85%E9%A1%B5%E9%9D%A2.png)

一路下一步，可以更改安装目录，我在虚拟机安装的就不改了。

![Git1](./../images/Git1.png)

可以把GitGUI取消了，这样看起来干净

![Git2](./../images/Git0.png)

完成安装

![image-20230313224044907](./../images/Git2.png)

在目标文件夹下，右键点击，使用Git bash Here后，输入git clone https://gitee.com/luoluooo/note命令，当然需要管理员先拉你进gitee组。

![](./../images/image-20230313224230564.png)

得到如下图所示![image-20230313224551445](./../images/Git3.png)
进入该文件夹，右键打开，点击Git Bash Here。

```bash
git remote add origin https://gitee.com/luoluooo/note  #建立远程链接，随后输入自己账号密码，后便可以正常上下传文件了，git基本命令看《常见软件的指令》
```

### git配置科学上网

```bash
git config --global -l #查看git配置
git config --global --unset.http.proxy  #重置git代理
git config --global http.proxy http://127.0.0.1:xxx #设置本地代理，xxx为代理开放端口号，在本地搜索找到【代理服务器设置】中配置，如下图。这里是10809
git config --global --unset http.proxy #取消代理

#使用socks5代理（推荐）
git config --global http.https://github.com.proxy socks5://127.0.0.1:51837
#使用http代理（不推荐）
git config --global http.https://github.com.proxy http://127.0.0.1:58591
```

![image-20230619111723135](../images/image-20230619111723135.png)
