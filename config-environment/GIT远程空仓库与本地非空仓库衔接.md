#### 远程空仓库与本地非空仓库衔接

```bash
#本机生成公密钥，并将本机公钥保存在服务器中
cd 本地仓库目录
git init   #初始化本地仓库，生成.git文件夹
git status #查看本地文件状态
git add .  #文件存入暂存区
git commit -m "xxx" #提交文件进入分支
git remote add origin https://gitee.com/luoluooo/note.git #建立远程空仓库与本地非空仓库链接
git push -u origin "master"  # 上传文件
```

##### 解决***git status***中文文件乱码

```bash
原因：在默认设置下，中文文件名在工作区状态输出，中文名不能正确显示，而是显示为八进制的字符编码。
解决办法
将git配置文件core.quotepath项设置为false。
quotepath表示引用路径
加上--global表示全局配置

git config --global core.quotepath false
```

##### 解决***git push*** 中 ***warning: could not find UI helper 'git-credential-manager-ui'***

```
原因：登录网站没有凭据，是git bug
解决方法:
打开win控制面板下的用户与账号下的凭据管理器，设置windows凭据管理下的普通凭据。
网络地址输入:https://gitee.com/
账号密码为登录gitee账号密码
```