#### open-chat-video-editor

https://github.com/SCUTlihaoyu/open-chat-video-editor打开链接，跟着教程走。



![image-20230519100908990](../images/image-20230519100908990.png)

将**requirements.txt**中的**numpy**安装版本删除，即**numpy==xxx** 改为**numpy**，避免版本冲突。

把**requirements.txt**中的**torch** **torchvision** **torchaudio**删除，第二步已经安装过了



![image-20230519101039291](../images/image-20230519101039291.png)

改为参考该教程安装cliphttps://blog.csdn.net/weixin_43392132/article/details/128200977



使用时需要挂着vpn；可能出现numpy.complex报错，找到对应文件改成numpy.complex128；chatGpt json文件中的api不需要引号