#### docker 安装

```bash
https://docs.docker.com/engine/install/ubuntu/ #docker官方安装教程
curl https://get.docker.com | sh && sudo systemctl --now enable docker
sudo usermod -aG docker $USER		#将当前USER用户添加到docker用户组

docker run --gpus all -it --rm -p 8888:8888 -v /home/luoluoluo:/workspace nvcr.io/nvidia/pytorch:xx.xx-py3  #nvidia docker 安装xx.xx: docker --version 23.0.3 => 23.03-py3 --gpus all：所有的显卡都映射到 docker 中 -it：interactive --rm：运行完之后删除
```





#### 小鱼docker一键安装ROS

```bash
wget http://fishros.com/install -O fishros && . fishros

sudo docker run -dit --name=ros-m -v /home/luo:/home/luo -v /tmp/.X11-unix:/tmp/.X11-unix --device=/dev/dri/renderD128 -v /dev/dri:/dev/dri --device /dev/snd -e DISPLAY=unix$DISPLAY -w /home/luo  fishros2/ros:melodic-desktop-full

docker exec -it ros-m /bin/bash -c "echo -e '
source /opt/ros/melodic/setup.bash' >> ~/.bashrc"  #容器写入环境变量

xhost +local:
```

