#### 删除鼠标右键-显示更多选项中的菜单项

**win+r** 输入**regedit**。在打开的注册表中找到：**HKEY_CLASSES_ROOT/Directory/Background/shell**，看到对应菜单项删除即可。

#### 删除电脑中多余的盘符

![image-20230313215638517](./../images/%E5%A4%9A%E4%BD%99%E7%9B%98%E7%AC%A6.png)

**win+r** 输入**regedit**。在打开的注册表中找到：**HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\MyComputer\NameSpace**，看到对应菜单项删除即可。

![image-20230313221100406](./../images/%E5%A4%9A%E4%BD%99%E7%9B%98%E7%AC%A6%E5%88%A0%E5%AE%8C%E5%90%8E.png)

#### win常见指令

win系统开机自启**exe**文件， 打开 ***cmd*** 输入**shell:startup** 将**exe**快捷方式放进去

***win***系统下 测试 ***MinGw***  打开 ***cmd*** 输入  ***gcc -v***

***win***系统下 测试 ***CMake***  打开 ***cmd*** 输入  ***cmake --version***



#### win11专业版 激活

```
https://zhuanlan.zhihu.com/p/629124034
```

#### win11修改c盘用户目录下的名字

```
https://zhuanlan.zhihu.com/p/440768641
```

#### win11专业版与家庭版区别

专业版可以跳Microsoft账号激活

#### 清理c盘操作(去除休眠功能)

```bash
#用管理员命令启动cmd
powercfg -h off #去除休眠功能
```

