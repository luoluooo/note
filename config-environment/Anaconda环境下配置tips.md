#### Anaconda pip安装目录

```bash
C:\Users\用户名\.conda\envs\环境名k\Lib\site-packages\pip
```

#### pip局部换源

```python
#阿里云 http://mirrors.aliyun.com/pypi/simple/ 
#中国科技大学 https://pypi.mirrors.ustc.edu.cn/simple/ 
#豆瓣(douban) http://pypi.douban.com/simple/ 
#清华大学 https://pypi.tuna.tsinghua.edu.cn/simple/ 
#中国科学技术大学 http://pypi.mirrors.ustc.edu.cn/simple/
pip install xxxx -i https://pypi.tuna.tsinghua.edu.cn/simple
```



#### pip全局换源

```python
pip config set global.index-url https://pypi.tuna.tsinghua.edu.cn/simple
```



#### miniconda安装

```bash
https://docs.conda.io/en/latest/miniconda.html  #miniconda官网
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh #下载
chmod +x Miniconda3-latest-Linux-x86_64.sh  #添加可执行权限
./Miniconda3-latest-Linux-x86_64.sh  #开始安装，一路回车和yes，除了选择安装路径的时候
```

