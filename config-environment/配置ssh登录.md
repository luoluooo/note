#### 配置ssh免密登录

生成公钥密钥对：***ssh-keygen -t rsa***(客户端)

```
sudo apt update
sudo apt install openssh-client
```

上传公钥到服务端：***ssh-copy-id 服务端用户名@服务端ip地址***  (不要随意更改github或gitee自动生成的对ssh公钥的备注) /将客户端的公钥复制到服务端的authorized_keys中去

#### ssh别名登录

```bash
~/.ssh/config(客户端)
Host 别名
  HostName IP地址
  User 用户名
  Port 22 (端口号)
```

#### Linux ssh 免密连windows

```
在windows设置中可选功能下下载openssh 客户端、openssh 服务端
在本机防火墙设置中新建入站规则，打开22号端口 	如果直接从设置中无法更改防火墙设置，从控制面板中的系统和安全中打开防火墙
更改windows下ssh的配置文件和authorized_keys安全配置，实现免密登录  注意权限 不要将当前用户设置进authorized_keys安全权限中，不然会出问题
在我的电脑属性中更改open ssh server设置自动启动
```

![image-20230720100042532](../images/image-20230720100042532.png)

![image-20230720100211727](../images/image-20230720100211727.png)

![image-20230720100121278](../images/image-20230720100121278.png)

![image-20230720100142793](../images/image-20230720100142793.png)

![img](../images/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBA6L-e6aOO6YO956yR5oiR5LqG,size_20,color_FFFFFF,t_70,g_se,x_16.png)