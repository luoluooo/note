```bash
进入https://github.com/Unity-Technologies/ml-agents/releases/tag/release_19下载ML-Agents
conda create -n unity python=3.8 #创建虚拟环境
conda activate unity	#进入虚拟环境
pip3 install torch~=1.7.1 -i https://pypi.doubanio.com/simple #安装pytorch
cd ML-Agents安装目录
pip3 install -e ./ml-agents-envs
pip3 install -e ./ml-agents
mlagents-learn --help  #查看是否安装成功
```

如果报

![image-20230305194157881](./../images/image-20230305194157881.png)

这类错误，重新安装protobuf

```bash
pip uninstall protobuf
pip install protobuf==3.20.1 -i https://pypi.doubanio.com/simple
```

如果报

![image-20230305194349781](./../images/image-20230305194349781.png)

这类错误，重新安装numpy

```bash
pip uninstall numpy
pip install -U numpy==1.23.5 -i https://pypi.doubanio.com/simple
```

***unity导入package***

![](./../images/image-20230305193900798.png)

```bash
mlagents-learn <trainer-config-file> --env=<env_name> --run-id=<run-identifier>
#开始训练 mlagents-learn config/poca/DungeonEscape.yaml --run-id=DungeonEscape
打开unity，按开始即可训练
```

