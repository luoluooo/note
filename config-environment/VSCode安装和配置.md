#### VSCode安装

小鱼一键安装

```bash
#以sudo用户身份运行下面的命令，更新软件包索引，并且安装依赖软件
sudo apt update
sudo apt install software-properties-common apt-transport-https wget
#使用wget命令插入Microsoft GPG key
wget -q https://packages.microsoft.com/keys/microsoft.asc -O- | sudo apt-key add -
#启用Visual Studio Code源仓库
sudo add-apt-repository "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main"
#一旦apt软件源被启用，安装Visual Studio Code软件包
sudo apt install code
#当一个新版本被发布时，在终端运行命令，来升级 Visual Studio Code 软件包
sudo apt update
sudo apt upgrade
```

#### VSCode配置ssh

```
https://zhuanlan.zhihu.com/p/412736012
```



#### 报Unable to write program user data错误

打开就报错, 根据报错找到/home/user/.Config/Code文件夹

用 ls -al 发现它的权限是这样的 drwxrwxrwx 11 root root 4096 3月  18 16:31 Code

我知道是权限问题了,把它从root权限变为当前用户权限

解决办法 sudo chown -R user:user Code  //前面user是用户名，后面是用户组

再打开VSCode就不报错了



#### 发现ubuntu下的VScode无法安装扩展软件时

根据提示把根目录下的.vscode配置文件删了，让其重新配置



#### VSCode自动在终端激活conda环境

```sh
ctrl + shift + p #打开搜索框，搜索Preferences:Open User Settings(JSON)
输入"python.terminal.activateEnvironment":True,
```

#### VSCode

修改新建终端类型

```
ctrl + , 打开设置
搜索terminal Default Profile
选择对应终端更改 如: cmd -> powershell
```



### 关闭vscode每次打开会自动加载上次的文件

```
settings=>window.restoreWindows=>none
settings=>files.hotExit=>off
```



#### VSCode命令

