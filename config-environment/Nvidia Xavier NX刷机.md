# Nvidia Xavier NX刷机

```bash
https://blog.csdn.net/zn_2580/article/details/125103936  #先将128G的固态变成系统盘，16G的内部存储变成外挂，避免内存不够的问题，根据经验来看，每次刷机都需要设置。这步的操作处于刷完系统后，系统自动开机，此时可以进行该操作和换源。
https://blog.csdn.net/qq_29477223/article/details/124102187 #刷机！
```

JetPack4.6对应的是ubuntu18.04，5.对应的是ubuntu20.04。可以全部勾选，进行刷机，Target Hardware勾普通版的，不勾开发者套件。

<img src="./../images/nvidia_Xavier_NX0.png" alt="img" style="zoom:75%;" />

若Automatic Setup不成功，切成manual Setup；如果事先进行了系统盘切换(即第一步链接中的操作)，则将Storage Device改成NVME。

![img](./../images/Nvidia_Xavier_NX1.png)

当target machine开机后，自己插上鼠标进行换源，不然的话，有一些组件无法下载成功。

```bash
wget http://fishros.com/install -O fishros && . fishros  #小鱼换源教程
sudo -H pip install jetson-stats
sudo jtop #使用上述命令可以查看NX硬件信息，看多核的cpu和gpu情况
```

