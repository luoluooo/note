#### 解决腾讯云ubuntu无法显示中文问题

简单的说是因为服务器没有安装***zh_CN.UTF-8*** 字符集，导致不支持中文！

***locale*** 执行这个命令，查看和语言编码有关的环境变量

```
LANG=
LANGUAGE=en_US:en
LC_CTYPE="POSIX"
LC_NUMERIC="POSIX"
LC_TIME="POSIX"
LC_COLLATE="POSIX"
LC_MONETARY="POSIX"
LC_MESSAGES="POSIX"
LC_PAPER="POSIX"
LC_NAME="POSIX"
LC_ADDRESS="POSIX"
LC_TELEPHONE="POSIX"
LC_MEASUREMENT="POSIX"
LC_IDENTIFICATION="POSIX"
LC_ALL=
```

可以看到大多数都默认为***POSIX*** 类型的，这时可以改服务器的默认字符集为***zh_CN.UTF-8***（我测试过改了没问题哦，可以解决汉字乱码问题。

##### 解决办法

```bash
sudo apt-get update   #ubuntu系统更新软件包列表
sudo apt-get install  -y language-pack-zh-hans
sudo apt-get install -y language-pack-zh-hant
cd /usr/share/locales 
sudo ./install-language-pack zh_CN  #开始安装zh_CN中文字符集 出现下图错误可忽略
sudo vim /etc/environment  #添加下面环境变量
source /etc/environment 
#重启(可选)
locale #检查
```

![img](..//images/v2-13ce354a528017ecd365847195094068_1440w.webp)

```bash
LANG=zh_CN.UTF-8
LANGUAGE=en_US:en
LC_CTYPE="zh_CN.UTF-8"
LC_NUMERIC="zh_CN.UTF-8"
LC_TIME="zh_CN.UTF-8"
LC_COLLATE="zh_CN.UTF-8"
LC_MONETARY="zh_CN.UTF-8"
LC_MESSAGES="zh_CN.UTF-8"
LC_PAPER="zh_CN.UTF-8"
LC_NAME="zh_CN.UTF-8"
LC_ADDRESS="zh_CN.UTF-8"
LC_TELEPHONE="zh_CN.UTF-8"
LC_MEASUREMENT="zh_CN.UTF-8"
LC_IDENTIFICATION="zh_CN.UTF-8"
LC_ALL=zh_CN.UTF-8
```

