#### Cuda与Cudnn安装
https://blog.csdn.net/h3c4lenovo/article/details/119003405
会默认安装在usr/local/cuda-xxx
多版本安装时不选择安装driver

使用ln-s建立不同cuda版本软连接，并在.bashrc输入以下语句，即完成多版本cuda切换
``` bash
export PATH="$PATH:/usr/local/cuda/bin"
export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:/usr/local/cuda/lib64/"
export LIBRARY_PATH="$LIBRARY_PATH:/usr/local/cuda/lib64"
```
