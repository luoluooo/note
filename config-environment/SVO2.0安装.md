### Install dependences

Install [catkin tools](https://catkin-tools.readthedocs.io/en/latest/installing.html) and [vcstools](https://github.com/dirk-thomas/vcstool) if you haven't done so before. Depending on your operating system, run

```bash
# For Ubuntu 18.04 + Melodic
sudo apt-get install python-catkin-tools python-vcstool
```

or

```bash
# For Ubuntu 20.04 + Noetic
sudo apt-get install python3-catkin-tools python3-vcstool python3-osrf-pycommon
```

Install system dependencies and dependencies for Ceres Solver

```bash
# system dep.
sudo apt-get install libglew-dev libopencv-dev libyaml-cpp-dev 
# Ceres dep.
sudo apt-get install libblas-dev liblapack-dev libsuitesparse-dev
```





### Clone and compile

Create a workspace and clone the code (`ROS-DISTRO`=`melodic`/`noetic`):

```bash
mkdir svo_ws && cd svo_ws
# see below for the reason for specifying the eigen path
catkin config --init --mkdirs --extend /opt/ros/<ROS-DISTRO> --cmake-args -DCMAKE_BUILD_TYPE=Release -DEIGEN3_INCLUDE_DIR=/usr/include/eigen3
cd src
# git clone git@github.com:uzh-rpg/rpg_svo_pro_open.git  
# vcs-import < ./rpg_svo_pro_open/dependencies.yaml
以上两步替换成把压缩包解压在src目录下
touch minkindr/minkindr_python/CATKIN_IGNORE
# vocabulary for place recognition
# cd rpg_svo_pro_open/svo_online_loopclosing/vocabularies && ./download_voc.sh
cd rpg_svo_pro_open/svo_online_loopclosing/vocabularies 
进入该目录后执行 wget http://rpg.ifi.uzh.ch/svo2/vocabularies.tar.gz -O - | tar -xz
cd ../../..
```

在开始编译之前，可以发现src/dbow2_catkin/CmakeList.txt中这样写道（同样的需要修改git地址）：

```bash
ExternalProject_Add(dbow2_src
  #GIT_REPOSITORY git@github.com:dorian3d/DBoW2.git
	GIT_REPOSITORY https://github.com/dorian3d/DBoW2.git
  CMAKE_ARGS -DCMAKE_INSTALL_PREFIX=${CATKIN_DEVEL_PREFIX}
  BUILD_COMMAND CXXFLAGS=-i${CATKIN_DEVEL_PREFIX}/include make
  INSTALL_COMMAND make install
)
```

编译

```catkin build -j6 #用六个线程加速编译
catkin build -j6 #用六个线程加速编译
```

