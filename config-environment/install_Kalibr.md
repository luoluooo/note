# Kalibr

## 一、简介

Kalibr是一种标定工具，该工具具有以下功能：

1、多相机标定
2、视觉-惯性标定(CAM-IMU):在相机系统中对IMU的空间和时间定标以及IMU的内在参数进行标定
3、多惯性校准(IMU-IMU):通过基本惯性传感器和IMU内在参数对IMU进行空间和时间校准(需要1辅助相机传感器)。
4、卷帘相机校准:卷帘相机的全本向校准(投影、失真和快门参数)。

## 二、安装方法

参考教程：官方教程https://github.com/ethz-asl/kalibr/wiki/installation

### 源码安装

#### 1、源码安装基于ROS，需要先安装ROS(包含OpenCV)

```sh
sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
sudo apt-key adv --keyserver 'hkp://keyserver.ubuntu.com:80' --recv-key C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654
sudo apt-get update
export ROS1_DISTRO=noetic # kinetic=16.04, melodic=18.04, noetic=20.04
sudo apt-get install ros-$ROS1_DISTRO-desktop-full
sudo apt-get install python-catkin-tools # ubuntu 16.04, 18.04
sudo apt-get install python3-catkin-tools python3-osrf-pycommon # ubuntu 20.04
```

#### 2、安装编译与运行依赖库

```sh
sudo apt-get install -y \
    git wget autoconf automake nano \
    libeigen3-dev libboost-all-dev libsuitesparse-dev \
    doxygen libopencv-dev \
    libpoco-dev libtbb-dev libblas-dev liblapack-dev libv4l-dev
```

对于不同的python版本：

```sh
# Ubuntu 16.04
sudo apt-get install -y python2.7-dev python-pip python-scipy \
    python-matplotlib ipython python-wxgtk3.0 python-tk python-igraph python-pyx
# Ubuntu 18.04
sudo apt-get install -y python3-dev python-pip python-scipy \
    python-matplotlib ipython python-wxgtk4.0 python-tk python-igraph python-pyx
# Ubuntu 20.04
sudo apt-get install -y python3-dev python3-pip python3-scipy \
    python3-matplotlib ipython3 python3-wxgtk4.0 python3-tk python3-igraph python3-pyx

```

#### 3、建立一个工作空间并克隆项目到src文件夹

```sh
mkdir -p ~/kalibr_workspace/src
cd ~/kalibr_workspace
export ROS1_DISTRO=noetic # kinetic=16.04, melodic=18.04, noetic=20.04
source /opt/ros/$ROS1_DISTRO/setup.bash
catkin init
catkin config --extend /opt/ros/$ROS1_DISTRO
catkin config --merge-devel # Necessary for catkin_tools >= 0.4.
catkin config --cmake-args -DCMAKE_BUILD_TYPE=Release
```

#### 4、使用Release配置编译代码，根据可用内存，您可能要减少编译线程数（例如添加 -j2到 catkin build）

```sh
cd ~/kalibr_workspace/
catkin build -DCMAKE_BUILD_TYPE=Release -j4
```

#### 5、编译成功后需要source工作空间的setup文件来使用Kalibr

```sh
source ~/kalibr_workspace/devel/setup.bash
rosrun kalibr <command_you_want_to_run_here>
```

## 三、使用

https://blog.csdn.net/qq_42703283/article/details/115067728?spm=1001.2101.3001.6650.5&utm_medium=distribute.pc_relevant.none-task-blog-2%7Edefault%7ECTRLIST%7ERate-5-115067728-blog-106204419.nonecase&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2%7Edefault%7ECTRLIST%7ERate-5-115067728-blog-106204419.nonecase&utm_relevant_index=6
