sudo docker run -dit \
    --name=yolocpp \
    --gpus all \
    --privileged \
    -p 20000:22 \
    -v /dev:/dev \
    -v /usr/local:/usr/local_mapping \
    -v /home/luoluoluo:/dockerMapping \
    -v /tmp/.X11-unix:/tmp/.X11-unix \
    -e DISPLAY=:1 \
    -w /home/luoluoluo \
    yolocpp:4.0 \
/bin/bash
