#### 实验室小车

```bash
#松灵机器人
rosrun tracer_bringup bringup_can2usb.bash		#刷新can
roslaunch tracer_bringup tracer_robot_base.launch	#启动小车底盘
roslaunch ouster_ros sensor.launch sensor_hostname:=<sensor host name> metadata:=<json file name>              # metadata is optional 雷达传感器模式
#实验室教育小车
ssh -Y wheeltec@192.168.0.100   #ssh登录教育小车
roslaunch turn_on_wheeltec_robot turn_on_wheeltec_robot.launch   #启动小车节点
sudo mount -t nfs 192.168.0.100:/home/wheeltec/wheeltec_robot /mnt #程序挂载
```

#### MLAgents

```bash
mlagents-learn config/ppo/3DBall.yaml --run-id=first3DBallRun --resume #--resume 继续上一次训练  --run-id 训练唯一标识
```