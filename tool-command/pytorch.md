#### torchvision

```python
import torchvision
from torchvision.transforms import Compose, RandomHorizontalFlip, ToTensor
from torchvision import transforms
# 数据集下载
trainset = torchvision.datasets.MNIST(root='./data', # 表示 MNIST 数据的加载的目录
                          train=True,  # 表示是否加载数据库的训练集，false的时候加载测试集
                          download=True, # 表示是否自动下载 MNIST 数据集
                          transform=None) # 表示是否需要对数据进行预处理，none为不进行预处理


transform_train = Compose([
    ToTensor(),  #转换为张量
    RandomHorizontalFlip()	#图像一半的概率翻转，一半的概率不翻转
])

# 数据预处理 2
trainset = torchvision.datasets.DatasetFolder(
    root='./data/cifar10/train', #Root directory path.
    loader=cv2.imread,           #A function to load a sample given its path.
    extensions=('png',),         #A list of allowed extensions. both extensions and is_valid_file should not be passed.
    transform=transform_train,   #A function/transform that takes in a sample and returns a transformed version.
    target_transform=None,       #A function/transform that takes in the target and transforms it.
    is_valid_file=None)          #A function that takes path of a file and check if the file is a valid file (used to check of corrupt files) both extensions and is_valid_file should not be passed.

from torchvision.transforms import functional as F
F.pil_to_tensor(img) #Convert a PIL Image to a tensor of the same type.

image = transforms.ToPILImage(mode="L")(x.float())
image.show()										#tensor转化为PIL图像，x为(1,height,weight)张量，且已经经过ToTensor()  L为灰度图，RGB为彩色图

```

#### Python import

```python
from .base import * #前面的.代表basePY文件在当前目录
from ..base import * #前面的..代表basePY文件在上一层目录

import sys, os
sys.path.append(os.path.join(os.getcwd(), 'core\models'))  #代表将xxx\core\models加入系统变量,以便之后可以import该目录下的curves
import curves
```

#### Jupyter

```python
pip install jupyter #安装jupyter
jupyter notebook	#启动jupyter
```

#### torch命令

```python
import torch
torch.__version__  #torch版本
torch.cuda.is_available()	#Gpu能否使用
torch.cuda.device_count()	#显卡数量
torch.cuda.get_device_name(0)	#显卡系列
torch.cuda.current_device()		#当前显卡编号
torch.cuda.get_device_capability(device=0) #查看GPU的容量
torch.cuda.current_stream(device=0) #查看当前cuda流
torch.cuda.get_device_properties(0) #查看显卡的属性
num = torch.cuda.device_count()
infos = [torch.cuda.get_device_properties(i) for i in range(num)]
print(infos) #输出所有显卡属性
nvidia-smi -i 0 -q #查看显卡所有信息 0为显卡索引


```



#### PIL

```python
#obj (numpy.ndarray): 一个二维numpy数组, 表示要转换为图像的数组。
#mode (str): 一个字符串, 表示输出图像的模式。常用的模式有 “L” (灰度图), “RGB” (彩色图), “CMYK” (Cyan, Magenta, Yellow, blacK)。
#将数组转换为灰度图像
img=Image.fromarray(obj,mode='L')
```



#### 常用代码

```python
import torch
from matplotlib import pyplot as plt
import numpy as np
import cv2 as cv
from PIL import Image
torch.set_printoptions(profile="full", sci_mode=False) # profile 输出完整张量  sci_mode 不使用科学计数法
np.set_printoptions(threshold=np.inf)  #输出完整numpy数组


torch.gt(abs(tensor),1)  #对张量的每个元素与1进行比较，大的返回ture，小的返回false

# 将my_resnet模型储存为my_resnet.pth
torch.save(my_resnet.state_dict(), "my_resnet.pth")
# 加载resnet，模型存放在my_resnet.pth
my_resnet.load_state_dict(torch.load("my_resnet.pth"))

#生成子图和调整子图格式
fig,axes = plt.subplots(5,5)
plt.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=0.2, hspace=0.4)
axes[i][j].set_title("xxxx") #设置子图标题


b = a.numpy() #tensor转numpy
b = torch.from_numpy(a) #numpy转tensor
b = Image.fromarray(cv2.cvtColor(a, cv2.COLOR_BGR2RGB)) #numpy转PIL
b = np.array(a)  #PIL转换为numpy

img = cv.imread(path) #读取图像
img = Image.open(patch).convert("RGB")	#PIL读取图像，并转化为RGB模式


newimg = Image.new('RGB', (320,320), color=(127,127,127)) #使用PIL生成RGB模式下大小为320，320，用RGB值为127，127，127的颜色填充的图片
newimg.paste(img, (xx, xx))	#将img图片粘贴在new_img像素以(xx,xx)位置为左上角的位置上
```



