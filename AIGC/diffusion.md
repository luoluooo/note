#### Tokenizer（分词器）

是将文本转换为单词或子词序列的过程。在自然语言处理中，文本通常是由一系列单词或子词组成的，而分词器的任务就是将这些单词或子词从文本中分离出来，并将它们转换为计算机可以处理的数字表示。

#### Embedding（词嵌入）

是将单词或子词转换为向量表示的过程。在自然语言处理中，单词或子词通常被表示为一个高维度的稀疏向量，其中每个维度对应一个单词或子词的特征。

#### 提示词

StableDiffusion是一款利用深度学习的文生图模型，支持通过使用提示词来产生新的图像，描述要包含或省略的元素。
我在这里引入StableDiffusion算法中的Prompt概念，又被称为提示符。
下面的prompt是用来指导AI绘画模型创作图像的。它们包含了图像的各种细节，如人物的外观、背景、颜色和光线效果，以及图像的主题和风格。这些prompt的格式经常包含括号内的加权数字，用于指定某些细节的重要性或强调。例如，"(masterpiece:1.5)"表示作品质量是非常重要的，多个括号也有类似作用。此外，如果使用中括号，如"{blue hair:white hair:0.3}"，这代表将蓝发和白发加以融合，蓝发占比为0.3。
以下是用prompt帮助AI模型生成图像的例子：masterpiece,(bestquality),highlydetailed,ultra-detailed,cold,solo,(1girl),(detailedeyes),(shinegoldeneyes),(longliverhair),expressionless,(long sleeves),(puffy sleeves),(white wings),shinehalo,(heavymetal:1.2),(metaljewelry),cross-lacedfootwear (chain),(Whitedoves:1.2)

仿照例子，给出一套详细描述以下内容的prompt。直接开始给出prompt不需要用自然语言描述：



#### StableDiffusion 基础工作流

使用文生图生成图片，选择喜欢的图片，通过controlnet的深度方法选取不同的风格。发送到涂鸦，使用取色器、橡皮擦等工具，对图片进行填充与消除





elevator:

photography, sleek and minimalist elevator lobby in a corporate building, front view, eye level shot, Canon EOS 5D Mark IV, EF 24mm f/1.4L II USM lens, f/2.8, vertical media art installation as the centerpiece, the art should exude a fluid, metallic sheen with shades of blue that ripple across its surface, providing a vibrant contrast to the understated elegance of the surroundings, lobby is defined by clean lines with a modern control gate system for secure access, materials used throughout are premium and carefully chosen for their texture and reflective qualities, enhancing the luxurious feel of the space, showcasing the height and grandeur of the lobby, with a focus on the harmony between technology and art, ultra resolution detail to emphasize the material finishes and the ambient lighting of the setting, realism light --ar 9:16 --style raw --stylize 700 --chaos 1 --v 6



dog:

P        A dog on the floor of an elevator,detailed fur texturem,full body,four feet on the ground,higher quality,master piece

N        (low quality,normal quality,worst quality),cropped,monochrome,lowres,low saturation,((watermark)),(white letters),skin spots,acnes,skin blemishes,age spot,mutated hands,mutated fingers,deformed,bad anatomy,disfigured,poorly drawn face,extra limb,ugly,poorly drawn hands,missing limb,floating limbs,disconnected limbs,out of focus,long neck,long body,extra fingers,fewer fingers,,(multi nipples),bad hands,signature,username,bad feet,blurry,bad body
