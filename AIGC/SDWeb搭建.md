#### 让SD-WEBUI(Stable-Diffution-WenUI) 实现外网访问，去除本地访问限制

本地化的SD-WEBUI(Stable-Diffution-WenUI)默认仅监听127.0.0.1的7860端口, docker映射端口

首先再程序根目录找到名为‘webui.py’的文件

```python
#在代码61行的位置，有以下一段代码，原始代码如下
if cmd_opts.server_name:
     server_name = cmd_opts.server_name
 else:
     server_name = "0.0.0.0" if cmd_opts.listen else None

#222行附近的一段代码     
api.launch(server_name="0.0.0.0" if cmd_opts.listen else "127.0.0.1", port=cmd_opts.port if cmd_opts.port else 7861)

#修改这两段代码，第一段如下：

if cmd_opts.server_name:
   server_name = cmd_opts.server_name
else:
   server_name = "0.0.0.0"

#第二段如下：

api.launch(server_name=server_name if server_name else "0.0.0.0", port=cmd_opts.port if cmd_opts.port else 7861) 
```

#### diffusion命令

```bash
./webui.sh  #启动webui命令
```

